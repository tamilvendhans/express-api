import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { BookingService } from '../../services/booking.service';
import { Router } from '@angular/router';
import { UserService } from '@app/services/user.service';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})
export class ProfileComponent implements OnInit {

  selectedTab: Number = 0;
  userData: any = {}
  successMsg: String;
  errMsg: String;
  activities: any = [];
  actMsg:String;

  constructor(
    private BookingService: BookingService,
    private route:Router,
    private userService: UserService
  ) { }

  ngOnInit() {
    this.userData = this.userService.getUserDataFromLocal();
    if(this.route.url.match('/activity')) {
      this.selectedTab = 1;
      this.setBookingsData();
    }
  }

  userForm(form: NgForm) {
    this.successMsg = '';
    this.errMsg = '';
    let name = form.value.user_name;
    let phone = form.value.user_mobile;
    let password = form.value.user_pwd;
    let gender = form.value.user_gender;
    let userId = this.userData._id;
    let user = {
      name,
      gender,
      password,
      phone,
      userId
    }

    this.userService.updateUser(user).subscribe((response) => {
      if(response.status == "1") {
        this.userService.setUserData(response.user);
        this.successMsg = response.message;
      } else {
        this.errMsg = response.message;
      }
    })
  }

  setBookingsData() {
    this.actMsg = '';
    let userId = this.userData._id;
    this.BookingService.getMyBookings({userId}).subscribe((response) => {
      if(response.message) {
        this.actMsg = response.message;
      }
      if(response.data) {
        this.activities = response.data;
      }
    })
  }

  changeTab(event) {
    if(event === 1) {
      this.setBookingsData();
    }
  }

  cancelTicket(bookingId) {
    if(confirm("Do you really want to cancel this ticket?")) {
      this.BookingService.cancelTrain({bookingId}).subscribe((response) => {
        if(response.status == 1) {
          let tempObj = this.activities;
          let index = tempObj.findIndex(item => item._id == bookingId);
          tempObj[index].status = 0;
          this.activities = tempObj;
        } else {
          alert("Something went wrong. Please try again later.")
        }
      })
    }
  }

}
