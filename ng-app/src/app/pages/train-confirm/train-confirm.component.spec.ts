import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrainConfirmComponent } from './train-confirm.component';

describe('TrainConfirmComponent', () => {
  let component: TrainConfirmComponent;
  let fixture: ComponentFixture<TrainConfirmComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrainConfirmComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrainConfirmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
