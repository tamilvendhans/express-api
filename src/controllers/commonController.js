const { TrainModel, EndPointsModel } = require('../models/db/trains.model');
const { deleteCollection } = require('../services/commonServices');

const commonRouter = require('express').Router();

commonRouter.get('/importSampleData', async(req, res) => {
    const TrainsCount = await TrainModel.collection.countDocuments();
    const EndpointsCount = await EndPointsModel.collection.countDocuments();
    if(TrainsCount == 0 && EndpointsCount == 0) {
        let sampleData = require('../trainsData.json');
        TrainModel.insertMany(sampleData.trains).then((TData) => {
            EndPointsModel.insertMany(sampleData['trains-endpoints']).then((EPData) => {
                res.send({'message': "Sample data imported successfully!", TData, EPData})
            }).catch((err) => {
                res.send({'message': 'Unable to create entries in trains-endpoints collection: ' + err.message})
            })
        }).catch((err) => {
            res.send({'message': 'Unable to create entries in trains collection: ' + err.message})
        })
    } else {
        res.send({'message': 'Seems DB already have the data.', TrainsCollectionsCount: TrainsCount, EndPointsCollectionsCount: EndpointsCount})
    }
})

commonRouter.get('/clearTrainsData', async(req, res) => {
    if(req.query.collection) {
        let collection = req.query.collection;
        try{
            let data = await deleteCollection(collection);
            res.send({ message: data });
        } catch(err) {
            res.send({'message': "Error: " + err.message});
        }
    } else {
        res.send({'message': 'collection query param missing.', 'syntax': '?collection=<collection_name>', 'exampleValue': '?collection=trains'})
    }

})

module.exports = commonRouter;